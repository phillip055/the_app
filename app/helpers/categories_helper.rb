module CategoriesHelper
	def categoryoptions
		s = ''
		Category.all.each do |category|
			s << "<option value = '#{category.id}'>#{category.name}</option>"
		end
		s.html_safe
	end
end
