class Post < ActiveRecord::Base
	belongs_to :user
	belongs_to :category
	validates :category, presence: true
	validates :title, presence: true
	validates :body, presence: true
	acts_as_commontable



	def self.search(query)
		where("title like ?", "%#{query}%")
	end




end